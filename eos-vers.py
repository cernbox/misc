#python -m trace --trace --ignore-module eosverutil,random,math,hashlib,__future__ eos-vers.py

# age-based file version expiry

# units
sec=1
minute=60
hour=60*minute
day=24*hour
month=30*day

# age slots: first slot = age<5minute, ...
v_age = [5*minute,1*hour,4*hour,1*day,7*day,1*month,2*month,6*month]

# number of version kept per slot
v_num = [3,2,1,1,1,1,1,1,1,1,1]

# total number of version copies
v_tot = sum(v_num)

###########

# run different activity patterns

def main():
    #bunch_of_work_every_day(180)
    #once_a_day(180)
    once_an_hour(1000)
    #very_often(10000)
    #less_active_in_time(10)
    #once_every_two_weeks(100)
    #alternating_random(100)

    print_final_status()

# UPDATE PATTERNS
        
def bunch_of_work_every_day(N):
    for i in range(N):
        once_an_hour(5) # work for five hours
        touch_file(1,1*day) # sleep until next day

def bunch_of_work_every_month(N):
    for i in range(N):
        once_an_hour(5)
        very_often(10)
        touch_file(1,1*month)        

def bunch_of_work_at_random(N):
    for i in range(N):
        touch_file(100,15,3*minute)
        touch_file(1,2*hour,50*day)
                
def less_active_in_time(N):
    touch_file(N,15)
    touch_file(N,2*minute)
    touch_file(N,6*minute)
    touch_file(N,13*minute)
    touch_file(N,1*hour)
    touch_file(N,2*hour)
    touch_file(N,1*day)
    touch_file(N,7*day)
    touch_file(N,30*day)
    touch_file(N,100*day)

# regular update patterns
def very_often(N):
    touch_file(N,16*sec)
            
def once_an_hour(N):
    touch_file(N,1*minute,1*hour)
    
def once_a_day(N):
    touch_file(N,16*hour,23*hour)

def once_every_two_weeks(N):
    touch_file(N,14*day)

def alternating_random(N):
    for i in range(N):
        touch_file(5,0,1*day)    
        touch_file(1,0,180*day)    


############

# sorted list of current versions of a file: this corresponds to the state retrievable by: eos ls #.sys.v.FILE
# most recent version (File entry) first
current_versions = []

# the versioning algorithm
#
# description:
#  - classify all existing versions into bins depending on their age
#  - find the oldest bin which exceeds the quota (quotas are set per bin)
#  - cleanup that bin: delete the one-but-last version in that bin.
#
# features:
#  * keep as many versions as possible, at minium the v_num versions per v_age slot
#  * this policy is applied at the time when new file version is created, at arbitrary moment in time the age of versions will differ
#  * in (rare) worst-case: guarantee to keep v_num-1 *most recent* versions entering v_age slot
#  *   in particular: v_age[0]=3 => always keep 2 most recent versions created
#  * the usual-case is generally much better and there will be more most recent file versions kept than this minimum
#  * it is a lazy algorithm: do the minimal number of deletions (rather then pedantic cleanup)
#  * laziness = some versions may be older than maximum allowed age


def add_version():
    global CNT_STAT_OPS, CNT_DEL_OPS

    # store the version first, worry about deletion of excess copies below
    current_versions.insert(0,File(current_time))

    if len(current_versions)<v_tot: # we stil have empty slots, no deletion
        return 

    CNT_STAT_OPS+=1
    if current_time-current_versions[-1].t > v_age[-1]: # last version is too old and falls off the cliff
        INFO("delete last version because it is too old: %s"%current_versions[-1])
        CNT_DEL_OPS+=1
        del current_versions[-1]
        return

    # if we are here then the last version is within the max specified age range
    
    K = len(v_age) # currently considered age slot, starting with oldest
    num = 0 # number of versions in the current age slot

    # iterate from oldest to newest version
    for i in reversed(range(len(current_versions))):
        
        DEBUG("version %d %d %s"%(i,K,current_versions[i]))

        # find age slot for currently considered version iterating backwards (oldest slot first)
        for k in reversed(range(K)): 
            
            if k == 0: # handle most recent (first) age slot
                age = 0
            else:
                age = v_age[k-1]

            DEBUG("slot %d age %d<t<%d"%(k,age,v_age[k]))

            CNT_STAT_OPS+=1
            if current_time-current_versions[i].t > age: # currently considered version fits into current slot k...
                if num+1>v_num[k]: # ... but number of versions in this slot exceeded
                    INFO("slot %d (%d<age<%d) capacity %d exceeded: delete %s"%(k,age,v_age[k],v_num[k],current_versions[i+num-1]))
                    CNT_DEL_OPS+=1
                    del current_versions[i+num-1] # delete the one but last version in this slot
                    return
                else:
                    DEBUG("increase version number in current slot %d"%k)
                    num+=1
                    K=k+1 # optimization: update K for the next version file to consider
                    # next version file will not be older than the current one because the list is sorted
                    # so no point in looking into previous age slots
                    break
            else:
                # our version belongs to one of preceding age slots
                # reset the counter and continue searching for slot (decrementing k)
                num = 0
                continue
                    
    assert(0) # should never reach here


# this represents a file entry in the namespace: it is actually just a tuple (creation_time,inode) with some python syntax sugar
# each time File is created the inode number is incremented so each version in uniquely identified by its sequence creation number
class File:
    SEQ = 0 # global file sequence number (inode)
    def __init__(self,t):
        self.inode=File.SEQ
        self.t=t
        File.SEQ += 1

    def __repr__(self):
        return "F%s(%s old)"%(repr(self.inode),pretty_time_delta(current_time-self.t))

# counters

CNT_STAT_OPS=0 # count how many time we fetch File entry from the namespace
CNT_DEL_OPS=0 # count how many time we delete File entry in the namespace 


###### UTIL #########

def DEBUG(x):
    return #print "DEBUG",x

def INFO(x):
    print x

def pretty_time_delta(seconds):
    seconds = int(seconds)
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return '%dd%dh%dm%ds' % (days, hours, minutes, seconds)
    elif hours > 0:
        return '%dh%dm%ds' % (hours, minutes, seconds)
    elif minutes > 0:
        return '%dm%ds' % (minutes, seconds)
    else:
        return '%ds' % (seconds,)


######### TESTING & SIMULATION ###########

# we simulate and advance current time, starting with 0
current_time = 0

def advance_time(t):
    global current_time
    current_time+=t
    print "clock+%-10s"%(pretty_time_delta(t))

def print_current_status():
    print "current time %-10s number of versions %d"%(pretty_time_delta(current_time), len(current_versions))
    print current_versions
    print

def print_final_status():
    print "%d file inodes %d stats (%.2f avg stats per created file version) %d deletes" % (File.SEQ,CNT_STAT_OPS,float(CNT_STAT_OPS)/File.SEQ,CNT_DEL_OPS)
    print
    print "algorithm parameters:"
    print "(v_age,v_num):", [("<"+pretty_time_delta(x),y) for x,y in zip(v_age,v_num)]
    print "total_max_number_of_versions", v_tot
    
import math
import random

def touch_file(N,a,b=None):
    for i in range(N):
        add_version()
        print_current_status()
        if b is None:
            step=a
        else:
            step = random.randint(a,b)
        advance_time(step)

main()

